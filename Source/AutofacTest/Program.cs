﻿using System;
using System.Linq;
using Autofac;

namespace AutofacTest
{

    enum CommandType
    {
        Add, Delete
    }

    interface ICommand<T>
    {
        void Execute(params T[] args);
    }

    class AddCommand<T> : ICommand<T>
    {
        public void Execute(params T[] args)
        {
            Console.WriteLine(" --- Adding ---");
            args.ToList()
                .ForEach(arg => Console.WriteLine(arg));
        }
    }

    public class DeleteCommand<T> : ICommand<T>
    {
        public void Execute(params T[] args)
        {
            Console.WriteLine(" --- Deleting ---");
            args.ToList()
                .ForEach(arg => Console.WriteLine(arg));
        }
    }

    class Program
    {
        static IContainer Config()
        {
            var container = new ContainerBuilder();

            container.RegisterType<AddCommand<int>>()
                .Keyed<ICommand<int>>(CommandType.Add);
            container.RegisterType<DeleteCommand<int>>()
                .Keyed<ICommand<int>>(CommandType.Delete);

            return container.Build();
        }

        static void Main(string[] args)
        {
            var container = Config();
            ICommand<int> addCommand = container.ResolveKeyed<ICommand<int>>(CommandType.Add);
            addCommand.Execute(1);
            ICommand<int> deleteCommand = container.ResolveKeyed<ICommand<int>>(CommandType.Delete);
            deleteCommand.Execute(1);
        }
    }
}
